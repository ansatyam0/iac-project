# Specify the AWS details
provider "aws" {
  region = "ap-south-1"
}

# Specify the EC2 details
resource "aws_instance" "example" {
  ami           = "ami-0e670eb768a5fc3d4"
  instance_type = "t2.micro"
}
# Create S3 bucket
resource "aws_s3_bucket" "example" {
  # NOTE: S3 bucket names must be unique across _all_ AWS accounts
  bucket = "satyamkabucket"
}
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}
